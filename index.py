import firebase_admin
import requests
from firebase_admin import credentials, firestore
from flask import Flask, request, Response, current_app, render_template, redirect, jsonify

app = Flask(__name__, static_url_path='/static')
# Use a service account
cred = credentials.Certificate('./mineral-bonus-93910-1fec6ead26fd.json')
firebase_admin.initialize_app(cred)

db = firestore.client()


@app.before_request
# Bit of a hack to use a catch-all URL, replace with individual calls if possible
def kibana_login_proxy():
    if 'static/img' not in request.url.replace(request.host_url, ''):
        if request.url.replace(request.host_url, '') == 'portal':
            return render_template('index.html')

        elif request.url.replace(request.host_url, '') == 'save-user':
            db.collection(u'users').document(request.form['uid']).set(request.form)
            return redirect('/portal')

        elif request.url.replace(request.host_url, '') == 'login':
            if request.method == 'POST':
                resp = requests.post(
                    url='https://efb9ee6663ae4ec8a11648b81a6edeec.us-east-1.aws.found.io:9243/api/security/v1/login',
                    headers={
                        'kbn-version': '6.5.4'
                    },
                    json={'username': 'analyst_2', 'password': 'analyst2019'}
                )

                excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
                headers = [(name, value) for (name, value) in resp.raw.headers.items()
                           if name.lower() not in excluded_headers]

                response = Response(resp.content, resp.status_code, headers)

                return response

            elif request.method == 'GET':
                return render_template('login.html')

        elif request.url.replace(request.host_url, '') == 'get-links':
            links_list = db.collection(u'links').get()
            for doc in links_list:
                print(u'{} => {}'.format(doc.id, doc.to_dict()))
            print(links_list)
            return jsonify(links_list)

        elif request.method == 'OPTIONS':
            options_resp = current_app.make_default_options_response()
            options_resp.headers.add('Access-Control-Allow-Origin', 'kp.dakshindia.org')
            options_resp.headers.add('Access-Control-Allow-Credentials', 'true')
            options_resp.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization, kbn-version')
            options_resp.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
            return options_resp

        else:
            resp = requests.request(
                method=request.method,
                url=request.url.replace(request.host_url,
                                        'https://efb9ee6663ae4ec8a11648b81a6edeec.us-east-1.aws.found.io:9243/'),
                headers={key: value for (key, value) in request.headers if key != 'Host'},
                data=request.get_data(),
                cookies=request.cookies,
                allow_redirects=False)

            excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
            headers = [(name, value) for (name, value) in resp.raw.headers.items()
                       if name.lower() not in excluded_headers]

            response = Response(resp.content, resp.status_code, headers)

            return response

# Context needed only for production. Comment the following line out and use adhoc ssl_context in development.
# context = ('/etc/letsencrypt/live/kp.dakshindia.org/cert.pem', '/etc/letsencrypt/live/kp.dakshindia.org/privkey.pem')
app.run(host='0.0.0.0', port=6900, debug=True, ssl_context='adhoc')
